#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys
print "os.getcwd()",os.getcwd()
#sentimentorigin=False
# if 'corpus_explorer' not in os.getcwd():#because of this buggy textmate...
# 	if 'sentiment_analysis' in os.getcwd():
# 		print 'je viens de la'
# 		sentimentorigin=True
# 		os.chdir('../corpus_explorer')
# 	else:
# 		os.chdir('corpus_explorer')

reload(sys)
sys.setdefaultencoding("utf-8")
from sqlite3 import *
from librarypy.path import *
from librarypy import fonctions
import sqlite3
import shutil
import logging, pprint
try:
	print 'user_parameters',user_parameters
except:
	user_parameters=''

parameters_user=fonctions.load_parameters(user_parameters)

#####PARAMETERS
data_source = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
operation=parameters_user.get('operation','')


fonctions.check_bddname(data_source)
filename_v = fonctions.get_data(data_source,'ISIpubdate',limit_id=1)
try:
	filename=filename_v.values()[0][0]['file']
except:
	filename='f'

#################################
###logging user parameters#######
#################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Script Data Curation Started')
yamlfile = 'data_curation.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext-methods/data_curation/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logging.info(parameterslog)
fonctions.progress(result_path0,2)



##################################
### end logging user parameters###
##################################
#####FUNC
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


#####CODE
print 'trying to open', data_source

conn,curs = fonctions.create_bdd(os.path.join(result_path,data_source))


fonctions.progress(result_path0,10)
logging.info('Loading data')

curs.execute("SELECT * FROM sqlite_master WHERE type='table'")
results = curs.fetchall()
existing_tables=map(lambda x:x[1],results)
print 'len(authorized_tables)',len(existing_tables)

print 'operation',operation

if operation=='Remove duplicate entries':
	variable_duplicate_table=parameters_user.get('duplicate_table','')
	print 'removing duplicate entries from', variable_duplicate_table

	redundant_ids=[]
	global_val={}
	for tablesource in [variable_duplicate_table]:
		for id,vald in fonctions.get_results(tablesource,curs, where=None,rank=True,unicity=False).iteritems():
			#if not id in global_dict:
			#	global_dict[id]={}
			#print id,vald
			for cle in vald.keys():
				if cle in global_val:
					redundant_ids.append(id)
				global_val[cle]=True
		logging.info('We have identified '+ str(len(redundant_ids))+ ' duplicate entries.')

	for table in existing_tables:
		if len(redundant_ids)>0:
			# delete all rows from table
			conn.execute('DELETE FROM ' + table + ' WHERE id IN (' + ', '.join(map(lambda x: str(x), redundant_ids))+')')
			logging.info('We have deleted records from the table '+table)
			#commit the changes to db
			conn.commit()
		#close the connection

	conn.execute("VACUUM")
if operation=='Delete variable(s)':
	variable_deletion_tables=parameters_user.get('variable_deletion_tables',[])
	print 'removing variables', variable_deletion_tables
	for table in variable_deletion_tables:
		sql_query = "drop table "+table
		print sql_query
		conn.execute(sql_query)
	conn.commit()
	conn.execute("VACUUM")
if operation=='Rename a variable':
	import re
	table_2be_renamed=parameters_user.get('variable_renaming','')
	variable_new_name=parameters_user.get('variable_new_name','xxx')
	variable_new_name=variable_new_name.replace('"','').replace("where","where_").replace("to","to_").replace("from","from_").replace(' ','_').replace('/','_').replace('-','_').replace('(','_').replace(')','_').replace('[','_').replace(']','_')
	variable_new_name=re.sub(r'\W+', '', variable_new_name)
	sql_query = "ALTER TABLE "+table_2be_renamed + " RENAME TO " + variable_new_name
	print sql_query
	conn.execute(sql_query)
	conn.commit()
	conn.execute("VACUUM")
if operation=='Concatenate variables':
	concatenate_tables=parameters_user.get('concatenate_tables',[])
	concatenate_tables_name=parameters_user.get('concatenate_tables_name','')
	if concatenate_tables_name=="":
		concatenate_tables_name = '_'.join(concatenate_tables)

	if concatenate_tables_name in existing_tables:
		logger = logging.getLogger()
		logging.debug('Table ' +concatenate_tables_name+' already in use, please change the new table name' )
		logger.handlers[0].flush()
		jdlkq
	concatenate_tables_style=parameters_user.get('concatenate_tables_style','categories')


	global_dict={}
	for tablesource in concatenate_tables:
		for id,vald in fonctions.get_results(tablesource,curs, where=None,rank=True,unicity=False).iteritems():
			if not id in global_dict:
				global_dict[id]={}
			#print id,vald
			for cle,rankd in vald.iteritems():
				for rank,parserank in rankd.iteritems():
					if not rank in global_dict[id]:
						global_dict[id][rank]={}
					for p in parserank:
						global_dict[id][rank].setdefault(p,[]).append(cle)
	global_dict_string={}
	lignes=[]
	if concatenate_tables_style=='string':
		concatenate_tables_separator=parameters_user.get('concatenate_tables_separator',' ')
	if 1:
		for id in global_dict.keys():
			for rank in global_dict[id].keys():
				for parserank in global_dict[id][rank].keys():
					#global_dict[id][rank][parserank]=
					if concatenate_tables_style=='string':
						lignes.append([filename,id,rank,parserank,concatenate_tables_separator.join(global_dict[id][rank][parserank])])
					else:
						for val in global_dict[id][rank][parserank]:
							lignes.append([filename,id,rank,parserank,val])
	try:
		logging.info('Creating the new table '+concatenate_tables_name)
		curs.execute('create table ' + concatenate_tables_name  + ' (file text, id integer, rank integer, parserank integer, data text)')
	except:
		pass
	sql_insert =  'INSERT OR IGNORE INTO ' + concatenate_tables_name+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	for ligne in lignes:
		curs.execute(sql_insert,ligne)
	logging.info('The new table was populated with '+str(len(lignes)) + ' entries.')
	conn.commit()

if operation=='Aggregating scores over every invidual document':
	convert_tables=parameters_user.get('convert_tables_average','')
	print 'convert_tables',convert_tables
	print "normal",parameters_user.get('convert_tables_average_normalization')
	if parameters_user.get('convert_tables_average_normalization',True):
		convert_table_to_name=convert_tables+'_average_score'
	else:
		convert_table_to_name=convert_tables+'_sum_score'
	print "convert_tables",convert_tables
	try:
		curs.execute('drop table '+convert_table_to_name)
		conn.commit()
	except:
		pass

	lignes=[]
	for id,vald in fonctions.get_results(convert_tables,curs, where=None,rank=True,unicity=False).iteritems():
		#print ('vald',vald)
		vald_total=[]
		for x, rp in vald.iteritems():
			for r, ps in rp.iteritems():
				for p in ps:
					vald_total.append(x)
		if parameters_user.get('convert_tables_average_normalization',True):
			lignes.append([filename,id,0,0,str(int(sum(map(lambda x: float(x),vald_total))/float(len(vald_total))))])
		else:
			lignes.append([filename,id,0,0,str(int(sum(map(lambda x: float(x),vald_total))))])
		#print ('[filename,id,0,0,str(int(sum(map(lambda x: float(x),vald.keys()))/float(len(vald))))]',[filename,id,0,0,str(int(sum(map(lambda x: float(x),vald.keys()))/float(len(vald))))])
	try:
		logging.info('Creating the new table '+convert_table_to_name)
		curs.execute('create table ' + convert_table_to_name  + ' (file text, id integer, rank integer, parserank integer, data text)')
	except:
		pass
	sql_insert =  'INSERT OR IGNORE INTO ' + convert_table_to_name+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	for ligne in lignes:
		curs.execute(sql_insert,ligne)
	print 'The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.'
	logging.info('The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.')
	conn.commit()

if operation=='Convert variable (into integer, time or string length)' and parameters_user.get('convert_table_to','time')=='length':
#	convert_tables=parameters_user.get('convert_tables','')

#if operation=='Measure the number of characters':
	convert_tables=parameters_user.get('convert_tables','')
	print 'convert_tables',convert_tables
	#convert_table_to_name=parameters_user.get('convert_table_to_name','')
	#if convert_table_to_name=='':
	convert_table_to_name1=convert_tables+'_length_characters'
	convert_table_to_name2=convert_tables+'_length_words'
	#convert_table_to_name2=convert_tables+'_length_words'
	print "convert_tables",convert_tables
	global_dict={}
	lignes1=[]
	lignes2=[]
	for id,vald in fonctions.get_results(convert_tables,curs, where=None,rank=True,unicity=False).iteritems():
		if not id in global_dict:
			global_dict[id]={}
		#print id,vald
		for cle,rankd in vald.iteritems():
			for rank,parserank in rankd.iteritems():
				if not rank in global_dict[id]:
					global_dict[id][rank]={}
				for p in parserank:
					try:
						lignes1.append([filename,id,rank,p,int(len(cle))])
						lignes2.append([filename,id,rank,p,int(len(cle.split()))])
					except:
						try:
							lignes1.append([filename,id,rank,p,len(cle.split()[0]) ])
							lignes2.append([filename,id,rank,p,len(cle.split()[0].split()) ])
						except:
							try:
								lignes1.append([filename,id,rank,p,len(cle.split()[-1] )])
								lignes2.append([filename,id,rank,p,len(cle.split()[-1].split() )])
							except:
								pass

	# if convert_table_to_name in existing_tables:
	# 	logger = logging.getLogger()
	# 	logging.debug('Table ' +convert_table_to_name+' already in use, please change the new table name' )
	# 	logger.handlers[0].flush()
	# 	jdlkq
	try:
		logging.info('Creating the new table '+convert_table_to_name1)
		logging.info('Creating the new table '+convert_table_to_name2)
		curs.execute('create table ' + convert_table_to_name1  + ' (file text, id integer, rank integer, parserank integer, data text)')
		curs.execute('create table ' + convert_table_to_name2  + ' (file text, id integer, rank integer, parserank integer, data text)')
	except:
		pass
	sql_insert1 =  'INSERT OR IGNORE INTO ' + convert_table_to_name1+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	sql_insert2 =  'INSERT OR IGNORE INTO ' + convert_table_to_name2+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	for ligne in lignes1:
		curs.execute(sql_insert1,ligne)
	for ligne in lignes2:
		curs.execute(sql_insert2,ligne)
	print 'The new table '+convert_table_to_name1+' was populated with '+str(len(lignes1)) + ' entries.'
	print 'The new table '+convert_table_to_name2+' was populated with '+str(len(lignes2)) + ' entries.'
	logging.info('The new table '+convert_table_to_name1+' was populated with '+str(len(lignes1)) + ' entries.')
	logging.info('The new table '+convert_table_to_name2+' was populated with '+str(len(lignes2)) + ' entries.')
	conn.commit()

if operation=='Convert variable (into integer, time or string length)' and parameters_user.get('convert_table_to','time')!='length' :
	convert_tables=parameters_user.get('convert_tables','')
	try:
		convert_tables_times=int(parameters_user.get('convert_tables_times','1'))
	except:
		convert_tables_times=1
	print 'convert_tables',convert_tables
	convert_table_to=parameters_user.get('convert_table_to','time')
	convert_table_to_name=parameters_user.get('convert_table_to_name','')
	if convert_table_to_name=='':
		if convert_table_to=="length":
			convert_table_to_name=convert_tables+'_length'
		else:
			convert_table_to_name=convert_tables+'_int'
	global_dict={}
	lignes=[]
	for id,vald in fonctions.get_results(convert_tables,curs, where=None,rank=True,unicity=False).iteritems():
		if not id in global_dict:
			global_dict[id]={}
		#print id,vald
		for cle,rankd in vald.iteritems():
			for rank,parserank in rankd.iteritems():
				if not rank in global_dict[id]:
					global_dict[id][rank]={}
				for p in parserank:
					try:
						if convert_table_to=="length":
							lignes.append([filename,id,rank,p,int(len(cle))])
						else:
							lignes.append([filename,id,rank,p,int(float(cle) *  convert_tables_times)])
					except:
						try:
							if convert_table_to=="length":
								lignes.append([filename,id,rank,p,len(cle.split()[0]) ])
							else:
								lignes.append([filename,id,rank,p,int(float(cle.split()[0]) *  convert_tables_times)])
						except:
							if convert_table_to=="length":
								try:
									lignes.append([filename,id,rank,p,len(cle.split()[-1] )])
								except:
									pass
							else:
								try:
									lignes.append([filename,id,rank,p,int(float(cle.split()[-1]) *  convert_tables_times)])
								except:
									pass


	if convert_table_to=='time':
		convert_table_to_name='ISIpubdate'
		curs.execute('drop table ISIpubdate')
		conn.commit()
	else:
		pass
		# if convert_table_to_name in existing_tables:
		# 	logger = logging.getLogger()
		# 	logging.debug('Table ' +convert_table_to_name+' already in use, please change the new table name' )
		# 	logger.handlers[0].flush()
		# 	jdlkq
	try:
		logging.info('Creating the new table '+convert_table_to_name)
		curs.execute('create table ' + convert_table_to_name  + ' (file text, id integer, rank integer, parserank integer, data integer)')

	except:
		pass
	sql_insert =  'INSERT OR IGNORE INTO ' + convert_table_to_name+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	for ligne in lignes:
		curs.execute(sql_insert,ligne)
	print 'The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.'
	logging.info('The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.')
	conn.commit()
if operation=='Split a Variable':
	convert_tables=parameters_user.get('variable_splitting','')
	print 'split_table',convert_tables
	variable_split_char=parameters_user.get('variable_split_char',';')
	convert_table_to_name=parameters_user.get('split_tables_name','').replace(' ','_').replace('.','_').strip()
	if convert_table_to_name=='':
		convert_table_to_name=convert_tables+'_split'
	global_dict={}
	lignes=[]
	for id,vald in fonctions.get_results(convert_tables,curs, where=None,rank=True,unicity=False).iteritems():
		if not id in global_dict:
			global_dict[id]={}
		#print id,vald
		for cle,rankd in vald.iteritems():
			for rank,parserank in rankd.iteritems():
				if not rank in global_dict[id]:
					global_dict[id][rank]={}
				for p in parserank:
					try:
						for h,cl in enumerate(cle.split(variable_split_char)):
							lignes.append([filename,id,rank,p+h,cl.strip()])
					except:
						pass
	if convert_table_to_name in existing_tables:
		logger = logging.getLogger()
		logging.debug('Table ' +convert_table_to_name+' already in use, please change the new table name' )
		logger.handlers[0].flush()
		jdlkq
	try:
		logging.info('Creating the new table '+convert_table_to_name)
		curs.execute('create table ' + convert_table_to_name  + ' (file text, id integer, rank integer, parserank integer, data text)')
	except:
		pass
	sql_insert =  'INSERT OR IGNORE INTO ' + convert_table_to_name+ " VALUES("  + ','.join(['?'] * (5)) + ")"
	for ligne in lignes:
		curs.execute(sql_insert,ligne)
	print 'The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.'
	logging.info('The new table '+convert_table_to_name+' was populated with '+str(len(lignes)) + ' entries.')
	conn.commit()
if operation=='Rename a Database':
	database_renaming=parameters_user.get('database_renaming','')
	database_renaming=database_renaming.replace(' ','_').replace('.','_').strip()
	bdd_name_new=os.path.join(result_path,database_renaming+'.db')

	from librarypy import descriptor
	import shutil
	print 'bdd_name_new',bdd_name_new
	shutil.copyfile(data_source, bdd_name_new)
	#print 'data_source',data_source

	from librarypy import descriptor
	try:
		print 'here we are'
		from librarypy import descriptor
		data=descriptor.get_descriptor(corpus_file)
		print 'data',data
		corpus_type=data['corpus_type']

	except:
		corpus_type=parameters_user.get('corpus_type','other')


	descriptor.generate(bdd_name_new,corpus_type,ix_directory='',schema={})




	logging.info('The database was renamed as '+database_renaming)
	conn.commit()


conn.close()

from librarypy import descriptor
descriptor.generate(data_source)
logging.info('Data curation script ended successfully')
fonctions.progress(result_path0,100)
